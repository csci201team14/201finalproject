package networking;

import java.io.Serializable;

public class PongScore implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public int p1score;
	public int p2score; 

	public PongScore(int p1, int p2)
	{
		p1score =p1;
		p2score = p2; 
	}
	
	public int getP1()
	{
		return p1score;
	}
	public int getP2()
	{
		return p2score;
	}
}
